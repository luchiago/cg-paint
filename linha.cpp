//******************************************************************************
//  Trabalho Rasterização
//
//  Professor: Prof. Laurindo de Sousa Britto Neto
//  Aluno: Lucas Hiago de Moura Vilela
//
//	Disclaimer: Para selecionar a ação desejada, clique com o botão direito
//	na tela para selecionar a opção
//  Não consegui implementar as letras h) e i).
//  
//	Instruções:
//  - Para desenhar uma linha: Clique com o botão direito, selecione a opção
//  da linha e clique em dois pontos na tela.
//  - Para desenhar um quadrado: Clique com o botão direito, selecione a opção
//  do quadrado e clique em dois pontos na tela.
//  - Para desenhar um triângulo: Clique com o botão direito, selecione a opção
//  do triângulo e clique em dois pontos na tela.
//  - Para desenhar um poligono: Clique com o botão direito, selecione a opção
//	do polígono e clique no minimo em 5 pontos na tela. Após isso, clique 
//  novamente com o botão direito e selecione a opção de imprimir o poligono na
//  tela
//  - Para fazer a translação: Desenhe uma forma, clique com o botão direito,
//  selecione a translação e o primeiro clique será na linha da forma e o
//  segundo no ponto para onde quer coloca-la
//  - Para fazer a escala: Desenhe uma forma, clique com o botão direito,
//	selecione a escala, clique na forma, aparecerá um input no terminal,
// 	digite o fator e será feita a transformação.
//  - Para fazer a reflexão: Desenhe uma forma, clique com o botão direito,
//  selecione a reflexão, clique na forma, aparecerá um input no terminal,
// 	digite em que eixo.
//  - Para fazer o cisalhamento: Desenhe uma forma, clique com o botão direito,
//	selecione o cisalhamento, clique na forma, aparecerá um input no terminal,
// 	digite o fator e o eixo e será feita a transformação.
//  - Para fazer o rotação: Desenhe uma forma, clique com o botão direito,
//	selecione o rotação, clique na forma, digite o angulo de rotação.
//  - Para sair: Clique no botão ESC
//******************************************************************************

// Bibliotecas utilizadas pelo OpenGL
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

// Biblioteca com funcoes matematicas
#include <cmath>

// Bibliotecas Auxiliares
#include <vector>
#include <iostream>
#include <cstdio>
#include <cstdlib>

// Constantes auxiliares
#define X1 0
#define X2 1
#define X3 2
#define Y1 0
#define Y2 1
#define Y3 2
#define POS_X1 0
#define POS_X2 1
#define POS_Y1 2
#define POS_Y2 3
#define POS_DELTA_X 0
#define POS_DELTA_Y 1

// Constantes das formas a serem desenhadas
#define NOT_FOUND -1
#define LINHA 1
#define QUADRADO 2
#define TRIANGULO 3
#define POLIGONO 4
#define IMPRIME_POLIGONO 5
#define TRANSLACAO 6
#define ESCALA 7
#define REFLEXAO 8
#define CISALHAMENTO 9
#define ROTACAO 10

using namespace std;

// Estruturas
// Estrututa de dados para o armazenamento dinamico dos pontos
// selecionados pelos algoritmos de rasterizacao
struct ponto{
    int x;
    int y;
    ponto * prox;
};

// Estrutura para armazenar as formas
struct forma{
	vector<ponto> pontos;
};

// Variaveis Globais
bool click_1 = false, click_2 = false; // Linhas e Quadrados
bool click_3 = false; // Triangulo
bool click_4 = false, click_5 = false; // Poligono
bool print_poligono = false; // Identificador para o botão direito
double x_1, y_1, x_2, y_2; // Coordenadas
int width = 512, height = 512; //Largura e altura da janela
int op = 0; // define a opção escolhida e linha por default
int localizacao_forma = NOT_FOUND; // procura de formas
vector<forma> formas; //vetor de formas
vector<ponto> points; //vetor de pontos

// Lista encadeada de pontos
// indica o primeiro elemento da lista
ponto * pontos = NULL;

// Funcao para armazenar um ponto na lista
// Armazena como uma Pilha (empilha)
ponto * pushPonto(int x, int y){
	ponto * pnt;
	pnt = new ponto;
	pnt->x = x;
	pnt->y = y;
	pnt->prox = pontos;
	pontos = pnt;
	return pnt;
}

// Funcao para desarmazenar um ponto na lista
// Desarmazena como uma Pilha (desempilha)
ponto * popPonto(){
	ponto * pnt;
	pnt = NULL;
	if(pontos != NULL){
		pnt = pontos->prox;
		delete pontos;
		pontos = pnt;
	}
	return pnt;
}

// Declaracoes forward das funcoes utilizadas
// Função que pega o input
void displayMenu();
// Função que irá tratar o input
void seleciona(int opcao);
void reshape(int w, int h);
void keyboard(unsigned char key, int x, int y);
void mouse(int button, int state, int x, int y);
void init(void);
void display(void);
// Função que percorre a lista de pontos desenhando-os na tela
void drawPontos();
// Função que implementa o Algoritmo Imediato para rasterizacao de retas
void retaImediata(double x1, double y1, double x2, double y2);
// Função que faz a troca de valores
void troca(int& a, int& b);
// Função que calcula os deltas
vector<int> calcular_deltas(vector<int> coordenadas);
// Função que implementa a Redução ao Primeiro Octante
vector<int> reducao_primeiro_octante(vector<int>& coordenadas, bool& declive, bool& simetrico);
// Função que implementa o Algoritmo de Bresenham
void bresenham(int x1, int x2, int y1, int y2);
// Função que desenha a Linha
void desenhaLinha();
// Função que desenha o Quadrado
void desenhaQuadrado();
// Função que desenha o Triangulo
void desenhaTriangulo();
// Função que desenha o Poligono
void desenhaPoligono();
// Função que procura a figura
int search(int x, int y);
// Função para a translação
void transformacaoTranslacao(int x_diff, int y_diff);
// Função para a escala
void transformacaoEscala();
// Função para a reflexão
void transformacaoReflexao();
// Função para cisalhamento
void transformacaoCisalhamento();
// Função para rotação
void transformacaoRotacao();


// Função Principal do C
int main(int argc, char** argv){
    glutInit(&argc, argv); // Passagens de parametro C para o glut
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB); // Selecao do Modo do Display e do Sistema de cor utilizado
    glutInitWindowSize (width, height);  // Tamanho da janela do OpenGL
    glutInitWindowPosition (100, 100); //Posicao inicial da janela do OpenGL
    glutCreateWindow ("Rasterizacao"); // Da nome para uma janela OpenGL
    init(); // Chama funcao init();
    displayMenu();
    glutReshapeFunc(reshape); //funcao callback para redesenhar a tela
    glutKeyboardFunc(keyboard); //funcao callback do teclado
    glutMouseFunc(mouse); //funcao callback do mouse
    glutDisplayFunc(display); //funcao callback de desenho
    glutMainLoop(); // executa o loop do OpenGL
    return 0; // retorna 0 para o tipo inteiro da funcao main();
}

void displayMenu(){
	int principal;
	principal = glutCreateMenu(seleciona);
	glutAddMenuEntry("Desenhar Linha", LINHA);
	glutAddMenuEntry("Desenha Quadrado", QUADRADO);
	glutAddMenuEntry("Desenha Triângulo", TRIANGULO);
	glutAddMenuEntry("Desenha Poligono", POLIGONO);
	glutAddMenuEntry("Imprime Poligono", IMPRIME_POLIGONO);
	glutAddMenuEntry("Translação", TRANSLACAO);
	glutAddMenuEntry("Escala", ESCALA);
	glutAddMenuEntry("Reflexão", REFLEXAO);
	glutAddMenuEntry("Cisalhamento", CISALHAMENTO);
	glutAddMenuEntry("Rotação", ROTACAO);
	glutAttachMenu(GLUT_RIGHT_BUTTON); // Clicar com o Botão Direito para aparecer o menu
}

void seleciona(int opcao){
	if (opcao == IMPRIME_POLIGONO){
		op = POLIGONO;
		print_poligono = true;
	} else{
		op = opcao;
		print_poligono = false;
	}
	glutPostRedisplay();
}

// Função com alguns comandos para a inicializacao do OpenGL;
void init(void){
    glClearColor(1.0, 1.0, 1.0, 1.0); //Limpa a tela com a cor branca;
}

void reshape(int w, int h)
{
	// Reinicializa o sistema de coordenadas
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Definindo o Viewport para o tamanho da janela
	glViewport(0, 0, w, h);
	
	width = w;
	height = h;
    glOrtho (0, w, 0, h, -1 ,1);  

	// muda para o modo GL_MODELVIEW (não pretendemos alterar a projecção
	// quando estivermos a desenhar na tela)
	glMatrixMode(GL_MODELVIEW);
	
	//Para redesenhar os pixes selecionados
	if (op == ESCALA || op == CISALHAMENTO){
		click_1 = true;
	} else if (op == LINHA || op == QUADRADO || op == TRANSLACAO){
		click_1 = true;
		click_2 = true;
	} else if(op == TRIANGULO){
		click_1 = true;
		click_2 = true;
		click_3 = true;
	} else if (op == POLIGONO){
		click_1 = true;
		click_2 = true;
		click_3 = true;
		click_4 = true;
		click_5 = true;
		print_poligono = true;
	}
}
// Função usada na funcao callback para utilizacao das teclas normais do teclado
void keyboard(unsigned char key, int x, int y){
    switch (key){ // key - variavel que possui valor ASCII da tecla precionada
        case 27: // codigo ASCII da tecla ESC
            exit(0); // comando pra finalizacao do programa
        break;
    }
}

//Funcao usada na funcao callback para a utilizacao do mouse
void mouse(int button, int state, int x, int y)
{
   switch (button){
      case GLUT_LEFT_BUTTON:
         if (state == GLUT_DOWN){
         	ponto point;
         	point.x = x;
         	point.y = height - y;
         	if (op == LINHA || op == QUADRADO){
				if(click_1){
                	click_2 = true;
                	x_1 = x;
                	y_1 = height - y;                	
                	points.push_back(point);
                	printf("X%d Y%d (X1 = %.0f Y1 = %.0f)\n", points.size(), points.size(), x_1, y_1);
                	glutPostRedisplay();
            	}else{
                	click_1 = true;
                	x_1 = x;
                	y_1 = height - y;
                	printf("X%d Y%d (X1 = %.0f Y1 = %.0f)\n", points.size() + 1, points.size() + 1, x_1, y_1);
                	points.push_back(point);
            	}
			} else if (op == TRIANGULO){
				if(click_2){
					click_3 = true;
					x_1 = x;
					y_1 = height - y;
					points.push_back(point);
					printf("X%d Y%d (X1 = %.0f Y1 = %.0f)\n", points.size(), points.size(), x_1, y_1);
					glutPostRedisplay();	
				} else if(click_1){
					click_2 = true;
					x_1 = x;
					y_1 = height - y;
					points.push_back(point);
					printf("X%d Y%d (X1 = %.0f Y1 = %.0f)\n", points.size(), points.size(), x_1, y_1);
				} else{
					click_1 = true;
					x_1 = x;
					y_1 = height - y;
					points.push_back(point);
					printf("X%d Y%d (X1 = %.0f Y1 = %.0f)\n", points.size(), points.size(), x_1, y_1);
				}
			} else if (op == POLIGONO && !print_poligono){
				if(click_4){
					click_5 = true;
	                x_2 = x;
	                y_2 = height - y;
	                points.push_back(point);
	                printf("X%d Y%d (X1 = %.0f Y1 = %.0f)\n", points.size(), points.size(), x_2, y_2);
				} else if(click_3){
	                click_4 = true;
	                x_2 = x;
	                y_2 = height - y;
	                points.push_back(point);
	                printf("X%d Y%d (X1 = %.0f Y1 = %.0f)\n", points.size(), points.size(), x_2, y_2);
	            } else if(click_2){
	                click_3 = true;
	                x_1 = x;
	                y_1 = height - y;
	                points.push_back(point);
	                printf("X%d Y%d (X1 = %.0f Y1 = %.0f)\n", points.size(), points.size(), x_1, y_1);               
	            } else if(click_1){
	                click_2 = true;
	                x_1 = x;
	                y_1 = height - y;
	                points.push_back(point);
	                printf("X%d Y%d (X1 = %.0f Y1 = %.0f)\n", points.size(), points.size(), x_1, y_1);								
				} else{
	                click_1 = true;
	                x_1 = x;
	                y_1 = height - y;
	                points.push_back(point);
	                printf("X%d Y%d (X1 = %.0f Y1 = %.0f)\n", points.size(), points.size(), x_1, y_1);							
				}
			} else if (op == POLIGONO && print_poligono){
				if(click_5){
					glutPostRedisplay();
				}
			} else if (op == TRANSLACAO){
				if(click_1){
					click_2 = true;
					x_2 = point.x;
					y_2 = point.y;
					glutPostRedisplay();			   					
				} else{
					click_1 = true;
					x_1 = point.x;
					y_1 = point.y;
				}
			} else if (op == ESCALA || op == REFLEXAO || op == CISALHAMENTO || op == ROTACAO){
				click_1 = true;
				x_1 = point.x;
				y_1 = point.y;
				glutPostRedisplay();
			}
         }
         break;
      default:
         break;
   }
}

// Funcao usada na funcao callback para desenhar na tela
void display(void){
    glClear(GL_COLOR_BUFFER_BIT); //Limpa o buffer de cores e reinicia a matriz
    glLoadIdentity();
    
    glColor3f (0.0, 0.0, 0.0); // Seleciona a cor default como preto
    if((op == LINHA || op == QUADRADO) && click_1 && click_2){
    	forma form;
    	formas.push_back(form);
        if (op == LINHA){
			desenhaLinha();
		} else{
			desenhaQuadrado();
		}
        drawPontos();
        click_1 = false;
        click_2 = false;
        points.clear();
    } else if(op == TRIANGULO && click_1 && click_2 && click_3){
        forma form;
        formas.push_back(form);        
		desenhaTriangulo();
		drawPontos();
        click_1 = false;
        click_2 = false;
        click_3 = false;
        points.clear();
	} else if(op == POLIGONO && click_1 && click_2 && click_3 && click_4 && click_5 && print_poligono){
    	forma form;
        formas.push_back(form);
        desenhaPoligono();
        drawPontos();
        click_1 = false;
        click_2 = false;
        click_3 = false;
        click_4 = false;
        click_5 = false;
        print_poligono = false;
        points.clear();
	} else if(op == TRANSLACAO && click_1 && click_2){
		localizacao_forma = search(x_1, y_1);
		transformacaoTranslacao(x_2 - x_1, y_2 - y_1);
		if(localizacao_forma != -1){
			drawPontos();
		}
		click_1 = false;
		click_2 = false;
		points.clear();
	} else if(op == ESCALA && click_1){
		localizacao_forma = search(x_1, y_1);
		transformacaoTranslacao(0 - x_1, 0 - y_1);
		transformacaoEscala();
		transformacaoTranslacao(x_1, y_1);
		if(localizacao_forma != -1){
			drawPontos();
		}
		click_1 = false;
	} else if(op == REFLEXAO && click_1){
		localizacao_forma = search(x_1, y_1);
		transformacaoReflexao();
		drawPontos();
		click_1 = false;
	} else if(op == CISALHAMENTO && click_1){
		localizacao_forma = search(x_1, y_1);
		transformacaoCisalhamento();
		drawPontos();
		click_1 = false;
	} else if(op == ROTACAO && click_1) {
		localizacao_forma = search(x_1, y_1);
		transformacaoRotacao();
		drawPontos();
		click_1 = false;
	}
	localizacao_forma = NOT_FOUND;
	drawPontos();
    glutSwapBuffers(); // manda o OpenGl renderizar as primitivas

}

//Funcao que desenha os pontos contidos em uma lista de pontos
void drawPontos(){
    ponto * pnt;
    pnt = pontos;
    
    // for necessário pois estamos lidando com vetores do C++.
    for(int i = 0; i < (int)formas.size(); i++){
		glBegin(GL_POINTS); // inicio do desenho
		for (int j = 0; j < (int)formas[i].pontos.size(); j++){
			glVertex2i(formas[i].pontos[j].x, formas[i].pontos[j].y);
		}
		glEnd(); // indica o fim do desenho
	}
}

void retaImediata(double x1,double y1,double x2,double y2){
    double m, b, yd, xd;
    double xmin, xmax,ymin,ymax;
    //Armazenando os extremos para desenho
    pontos = pushPonto((int)x1,(int)y1);
    pontos = pushPonto((int)x2,(int)y2);

    if(x2-x1 != 0){ //Evita a divisão por zero
        m = (y2-y1)/(x2-x1);
        b = y1 - (m*x1);

        if(m>=-1 && m <= 1){ // Verifica se o declive da reta tem tg de -1 a 1, se verdadeira calcula incrementando x
            xmin = (x1 < x2)? x1 : x2;
            xmax = (x1 > x2)? x1 : x2;

            for(int x = (int)xmin+1; x < xmax; x++){
                yd = (m*x)+b;
                yd = floor(0.5+yd);
                pontos = pushPonto(x,(int)yd);
            }
        }else{ // Se menor que -1 ou maior que 1, calcula incrementado os valores de y
            ymin = (y1 < y2)? y1 : y2;
            ymax = (y1 > y2)? y1 : y2;

            for(int y = (int)ymin + 1; y < ymax; y++){
                xd = (y - b)/m;
                xd = floor(0.5+xd);
                pontos = pushPonto((int)xd,y);
            }
        }

    }else{ // se x2-x1 == 0, reta perpendicular ao eixo x
        ymin = (y1 < y2)? y1 : y2;
        ymax = (y1 > y2)? y1 : y2;
        for(int y = (int)ymin + 1; y < ymax; y++){
            pontos = pushPonto((int)x1,y);
        }
    }
}

void troca(int& a, int& b){
	int temp;
	
	temp = a;
	a = b;
	b = temp;
}

vector<int> calcular_deltas(vector<int> coordenadas){
	vector<int> deltas;
	
	// Delta X será a posição 0 do vetor
	deltas.push_back(coordenadas[POS_X2]-coordenadas[POS_X1]);
	
	// Delta Y será a posição 1 do vetor
	deltas.push_back(coordenadas[POS_Y2]-coordenadas[POS_Y1]);
	
	return deltas;
}

vector<int> reducao_primeiro_octante(vector<int>& coordenadas, bool& declive, bool& simetrico){
	vector<int> deltas;
	int produto;
	
	deltas = calcular_deltas(coordenadas);
	produto = deltas[POS_DELTA_X] * deltas[POS_DELTA_Y];
	
	// Teste de Produto
	if (produto < 0){
		coordenadas[POS_Y1] *= -1;
		coordenadas[POS_Y2] *= -1;
		deltas[POS_DELTA_Y] *= -1;
		simetrico = true;
	}
	
	// Teste Valor Absoluto
	if (fabs(deltas[POS_DELTA_X]) < fabs(deltas[POS_DELTA_Y])){
		troca(coordenadas[POS_X1], coordenadas[POS_Y1]);
		troca(coordenadas[POS_X2], coordenadas[POS_Y2]);
		troca(deltas[POS_DELTA_X], deltas[POS_DELTA_Y]);
		declive = true;
	}
	
	// Teste X1 e X2
	if (coordenadas[POS_X1] > coordenadas[POS_X2]){
		troca(coordenadas[POS_X1], coordenadas[POS_X2]);
		troca(coordenadas[POS_Y1], coordenadas[POS_Y2]);
		deltas[POS_DELTA_X] *= -1;
		deltas[POS_DELTA_Y] *= -1;
	}
	
	return deltas;
}

void bresenham(int x1, int x2, int y1, int y2){
	bool declive = false; 
	bool simetrico = false;
	vector<int> deltas;
	vector<int> coordenadas;
	
	coordenadas.push_back(x1);
	coordenadas.push_back(x2);
	coordenadas.push_back(y1);
	coordenadas.push_back(y2);
	
	deltas = reducao_primeiro_octante(coordenadas, declive, simetrico);

	int d = 2*deltas[POS_DELTA_Y] - deltas[POS_DELTA_X];
	int incE = 2*deltas[POS_DELTA_Y];
	int incNE = 2*(deltas[POS_DELTA_Y] - deltas[POS_DELTA_X]);
	
	pontos = pushPonto(coordenadas[POS_X1],coordenadas[POS_X2]);
	pontos = pushPonto(coordenadas[POS_Y1],coordenadas[POS_Y2]);
	ponto pont;
	
	while(coordenadas[POS_X1] < coordenadas[POS_X2]){
		int x = coordenadas[POS_X1];
		int y = coordenadas[POS_Y1];
		
		if (declive){
			troca(x, y);
		}
		
		if (simetrico){
			y *= -1;
		}
		
		if(d <= 0){
			d += incE;
			pontos = pushPonto(x, y);
			pont.x = x;
			pont.y = y;
			formas[formas.size() - 1].pontos.push_back(pont);
		} else{
			d += incNE;
			pontos = pushPonto(x, y);
			pont.x = x;
			pont.y = y;
			formas[formas.size() - 1].pontos.push_back(pont);
			coordenadas[POS_Y1] += 1;
		}
		coordenadas[POS_X1] += 1;
	}
}

void desenhaLinha(){
	bresenham(points[X1].x, points[X2].x, points[Y1].y, points[Y2].y);
}

void desenhaQuadrado(){
	bresenham(points[X1].x, points[X2].x, points[Y1].y, points[Y1].y);
	bresenham(points[X1].x, points[X1].x, points[Y1].y, points[Y2].y);
	bresenham(points[X2].x, points[X1].x, points[Y2].y, points[Y2].y);
	bresenham(points[X2].x, points[X2].x, points[Y2].y, points[X1].y);
}

void desenhaTriangulo(){
	bresenham(points[X1].x, points[X2].x, points[Y1].y, points[Y2].y);
	bresenham(points[X1].x, points[X3].x, points[Y1].y, points[Y3].y);
	bresenham(points[X2].x, points[X3].x, points[Y2].y, points[Y3].y);
}

void desenhaPoligono(){
	for (int i = 0 ; i < (int)points.size(); i++){
		if((int)points.size() - 1 == i)
			bresenham(points[i].x, points[0].x, points[i].y, points[0].y);	
		else
			bresenham(points[i].x, points[i + 1].x, points[i].y, points[i + 1].y);	
	}
}

int search(int x, int y){
	for(int i = 0; i < (int)formas.size(); i++){
		for(int j = 0; j < (int)formas[i].pontos.size(); j ++){
			if (((formas[i].pontos[j].x + 5) >= x_1) && (x_1 >= (formas[i].pontos[j].x - 5))){
				if (((formas[i].pontos[j].y + 5) >= y_1) &&  (y_1 >= (formas[i].pontos[j].y - 5))){
					return i;
				}
			}
		}
	}
	printf("\n*****FIGURA NÃO ENCONTRADA*******\n");
	return -1;
}

void transformacaoTranslacao(int x_diff, int y_diff){
	if (localizacao_forma != NOT_FOUND){
		for(int i = 0; i < (int)formas[localizacao_forma].pontos.size(); i++){
			formas[localizacao_forma].pontos[i].x += x_diff;
			formas[localizacao_forma].pontos[i].y += y_diff;	
		}
	}
}

void transformacaoEscala(){
	float fator;
	printf("\nFator de escala: ");
	scanf("%f", &fator);
	if (localizacao_forma != NOT_FOUND){
		for(int i = 0; i < (int)formas[localizacao_forma].pontos.size(); i++){
			formas[localizacao_forma].pontos[i].x *= fator;
			formas[localizacao_forma].pontos[i].y *= fator;
		}
	}	
}

void transformacaoReflexao(){
	int choice, x_ref, y_ref, x_translacao, y_translacao;
	printf("\nDigite a forma da reflexao(1 - Em x, 2 - Em y, 3 - Em ambos): ");
	scanf("%d", &choice);
	
	if(choice == 1) {
		x_ref = 1;
		y_ref = -1;
		x_translacao = 0;
		y_translacao = 3 * y_1 / 2;
	} else if(choice == 2){
		x_ref = -1;
		y_ref = 1;
		x_translacao = 3 * x_1 / 2;
		y_translacao = 0;
	} else if(choice == 3){
		x_ref = -1;
		y_ref = -1;
		x_translacao = 3 * x_1 / 2;
		y_translacao = 3 * y_1 / 2;
	}
	
	if (localizacao_forma != NOT_FOUND){
		for(int i = 0; i < (int)formas[localizacao_forma].pontos.size(); i++){
			formas[localizacao_forma].pontos[i].x *= x_ref;
			formas[localizacao_forma].pontos[i].y *= y_ref;
		}
		transformacaoTranslacao(x_translacao, y_translacao);
	}	
}

void transformacaoCisalhamento(){
	int choice;
	float fator;
	printf("\nDigite o fator de cisalhamento: ");
	scanf("%f", &fator);
	printf("\nPara cisalhamento em x, digite 1. Para cisalhamento em y, digite 2: ");
	do {
		scanf("%d", &choice);   	
	} while(choice != 1 && choice != 2);
	
	if (localizacao_forma != NOT_FOUND){
		transformacaoTranslacao(0 - x_1, 0 - y_1);
		
    	for(int i = 0; i < (int)formas[localizacao_forma].pontos.size(); i++){
			if(choice == 1) {
				formas[localizacao_forma].pontos[i].x += fator*formas[localizacao_forma].pontos[i].y;
			}
			if(choice == 2) {
				formas[localizacao_forma].pontos[i].y += fator*formas[localizacao_forma].pontos[i].x;
			}
		}
		
		transformacaoTranslacao(x_1, y_1);
	}
}

void transformacaoRotacao(){
	float angulo;
	
	printf("\nDigite o angulo de rotacao: ");
	scanf("%f", &angulo);
	if (localizacao_forma != NOT_FOUND){
		transformacaoTranslacao(0 - x_1, 0 - y_1);
		
		for(int i = 0; i < (int)formas[localizacao_forma].pontos.size(); i++){
			formas[localizacao_forma].pontos[i].x = (formas[localizacao_forma].pontos[i].x * cos(angulo)) - (formas[localizacao_forma].pontos[i].y * sin(angulo));
			formas[localizacao_forma].pontos[i].y = (formas[localizacao_forma].pontos[i].x * sin(angulo)) + (formas[localizacao_forma].pontos[i].y * cos(angulo));
		}
		
		transformacaoTranslacao(x_1, y_1);
	}
}
